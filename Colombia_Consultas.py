# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 15:09:04 2019
Last Modified May 10 10:00:00 2020 #Se añadió un segmento para consultas geográficas a partir de polígonos
Last Modified May 10 10:00:00 2022 #Ajustes en documenación y ejemplos.

@authors: ricardo.ortiz 


#Consultas

# Este script permite resolver consultas taxonómicas y geográficas de los datos publicados en el SiB Colombia de forma rápida

#1. Consulta Taxonómica
#2. Consulta por departamento
#3. Consulta por munucipio
#4. Consulta Geográfica
#5. Consulta por fecha de evento
#6. Consulta por DOI
"""

#Importar paquetes necesarios
import pandas as pd
import geopandas as gpd
from shapely.geometry import Point


#Cargar el dataset con los datos de Colombia previamente espacializados e intersecados con el MGN del DANE
dwc_co = pd.read_csv('C:\\Cifras_Colombia\\2021\\Datos_Trimetrales\\TrimestreIV\\dwc_co_2021T4.txt', encoding = "utf8", sep="\t")#2021T4


#Ingresar datos según el tipo de consulta

#Constula por departamentos. Defina en la variable d los departamentos que quiere consultar.
d= 'La Guajira', 'Sucre', 'Atlántico', 'Bolívar', 'Magdalena', 'Cesar'
#Consulta por municipios. Defina en la variable m los municipios que quiere consultar.
m= 'Paratebueno', 'Villanueva', 'Barranca de Upía', 'Cabuyaro' 
#Consulta por localidad. Defina en la variable l las localidades que quiere consultar.
l= 'Humedal Capellanía', 'Humedal Capellania', 'Humedal Capellania, Bogotá', 
#Consulta por zona marítima. Ingrese el nombre la zona martítima. Para estar seguro de los nombres de las zonas en el Corte de dato use 
#dwc_co.groupby(['ZonaMaritima']).gbifID.nunique().reset_index()
zm = 'Mar Caribe'
#Consultas por año del evento. Ingrese el año a consultar
f= 2019
#Consulta por doi. Ingrese la lista de DOI's a consultar
doi= "https://doi.org/10.15472/iqnpse" 


'''
Las consultas pueden ser anidadas, taxonomía, geografía (departamento), geografía (municipio). Siga los pasos viendo los comentarios.
'''

#---------------------------------------------1. Consulta taxonómica ---------------------------------------------------------------------------

# Si las consultas son taxonómicas, usar #1. CONSULTA TAXONOMICA para generar búsqueda por taxonomía.
# Tener en cuenta si la consulta incluye varios taxa deben generarse subsets y luego concatenarlos: 
    # ej. Kingdom: Plantae | class: Aves, Mammalia, Reptilia. 


## Consulta para un ÚNICO taxa filtrable a partir de  un único elemento DwC Ejemplo: Plantas en el elemento DwC kingdom
taxo = dwc_co[(dwc_co['kingdom'] == 'Plantae' )]

## Consulta para VARIOS taxa filtrable a partir de  un único elemento DwC Ejemplo: Aves, Mammalia y Reptilia, en el elemento DwC Class.
taxo = dwc_co[dwc_co['class'].isin(['Aves','Mammalia', 'Reptilia'])]



# Si debe combinar varios taxa, por ejemplo: todas las plantas, más  aves, mamíferos y reptiles (En diferentes elementos DwC). 

taxo1 = dwc_co[(dwc_co['kingdom'] == 'Plantae' )]
taxo2 = dwc_co[dwc_co['class'].isin(['Aves','Mammalia', 'Reptilia'])]
taxo = pd.concat([taxo1,taxo2], sort=False)
taxo = taxo.drop_duplicates('gbifID',inplace=False)


#---------------------------------------------2. Consulta por departamento -----------------------------------------------------------------------

## Consultas CON requerimientos taxonómicos

    ### Un solo departamento
dpto_g= taxo[(taxo['Departamento-ubicacionCoordenada']== d)]
dpto_t = taxo[(taxo['stateProvince']== d)]
dpto=pd.concat([dpto_g,dpto_t], sort=False)
dpto=dpto.drop_duplicates('gbifID',inplace=False)

    ###Varios departamentos
dpto_g = taxo[taxo['Departamento-ubicacionCoordenada'].isin(d)]
dpto_t= taxo[taxo['stateProvince'].isin(d)]
dpto=pd.concat([dpto_g,dpto_t], sort=False)
dpto=dpto.drop_duplicates('gbifID',inplace=False)


## Consultas SIN requerimientos taxonómicos

    ### Un solo departamento
dpto_t = dwc_co[(dwc_co['stateProvince']== d)]
dpto_g = dwc_co[(dwc_co['Departamento-ubicacionCoordenada']== d)]
dpto=pd.concat([dpto_g,dpto_t], sort=False)
dpto=dpto.drop_duplicates('gbifID',inplace=False)

    ### Varios departamentos
dpto_t = dwc_co[dwc_co['stateProvince'].isin(d)]
dpto_g = dwc_co[dwc_co['Departamento-ubicacionCoordenada'].isin(d)]
dpto=pd.concat([dpto_g,dpto_t], sort=False)
dpto=dpto.drop_duplicates('gbifID',inplace=False)

   ### Zona Maritima
zmaritima = dwc_co[(dwc_co['ZonaMaritima']== zm)]
# Unir el resultado de la consulta marina con los datos por departamento.
dptozm=pd.concat([dpto,zmaritima], sort=False)
dptozm=dptozm.drop_duplicates('gbifID',inplace=False)





#---------------------------------------------3. Consulta por municipios -----------------------------------------------------------------------

## Para evitar errores de sinónimos entre municipios, generar un subset de departamentos y luego seleccionar los municipios de interés

    ### Un solo municipio
mpios_g = dpto[(dpto['Municipio-ubicacionCoordenada']== m)]
mpios_t = dpto[(dpto['county']== m)]
mpios=pd.concat([mpios_g,mpios_t], sort=False)
mpios=mpios.drop_duplicates('gbifID',inplace=False)

    ### Varios municipios
mpios_g = dpto[dpto['Municipio-ubicacionCoordenada'].isin(m)]
mpios_t = dpto[dpto['county'].isin(m)]

mpios=pd.concat([mpios_g,mpios_t], sort=False)
mpios=mpios.drop_duplicates('gbifID',inplace=False)


  ### Localidad 
# En esta consulta es mejor primero tener un subset por departamenta para facilitar la consulta. Por eso se realiza a partir del resultado de ejecutar dpto.  
  
    ### Una sola localidad
locality = dpto[dpto['locality'].str.contains(l, na=False)]
    ### Varias localidades
locality = dpto[dpto['locality'].isin(l)]

#---------------------------------------------4. Consulta geográfica - Cruce Polígono-----------------------------------------------------------

# Para optimizar el cruce geográfico, de ser posible, realice prefiltros de departamento y municipio. Según haya sido el caso seleccione
# una de las siguientes opciones.



#Nacional
geo = dwc_co
#Filtro Departamental
geo = dpto
#Filtro Municipal
geo = mpios


#Ejecute las siguientes líneas apra convertir el dataframe en un geodataframe. De este modo podrá hacer las consultas geográficas.
geo[['decimalLatitude', 'decimalLongitude']] = geo[['decimalLatitude','decimalLongitude']].fillna(value=0)
geo['Coordinates'] = list(zip(geo.decimalLongitude, geo.decimalLatitude))
geo['Coordinates'] = geo['Coordinates'].apply(Point)
geo = gpd.GeoDataFrame(geo, geometry='Coordinates')
geo.crs = {'init' :'epsg:4326'}



#OPCION 1 - CRUCES SIMPLES (geo no superior a 3.000.000 de registros)

#El script contien el código para un ejemplo de cruce con dos diferetes polígonos, para mas polígonos copie y pegue cambiando la ubicación del archivo
#Importar archivo shapefile (revisar la codificación de la capa y el sistema de referencia) preferiblemente que esté ya en WGS84, sino hay que realizar una conversión para que funcione el spatialJoin
polygon1 = gpd.read_file('C:\\Users\\ricardo.ortiz\\Documents\\Consultas_Histórico\\Consultas_2022\\Cifras-Orinoquia\\Orinoquia.shp', encoding = 'utf8')


#Realizar cruces geográfico entre geodataframe y el polígono
geo_j = gpd.sjoin(geo, polygon1, how="left", op="intersects")

#Filtre por nombre columna los elementos que no hayan quedado vacíos, ese será el resultado del cruce geográfico. Remplace NombreColumna por el nombre del alguna columna del shapefile
geo_r = geo_j[geo_j.NombreColumna.notnull()]




#OPCION 2 - CRUCES COMPLEJOS (geo superiores a 3.000.000 de registros)

#Para cruces grandes es mejor simplificar el dataset y usar solo las columnas necesarias para el Join espacial. Luego realice un merge entre el resultado y los demás datos.
geo_simple = geo[['gbifID', 'decimalLatitude', 'decimalLongitude', 'species' ]]

#Ejecute las siguientes líneas apra convertir el dataframe en un geodataframe. De este modo podrá hacer las consultas geográficas.
geo_simple[['decimalLatitude', 'decimalLongitude']] = geo_simple[['decimalLatitude','decimalLongitude']].fillna(value=0)
geo_simple['Coordinates'] = list(zip(geo_simple.decimalLongitude, geo_simple.decimalLatitude))
geo_simple['Coordinates'] = geo_simple['Coordinates'].apply(Point)
geo_simple = gpd.GeoDataFrame(geo_simple, geometry='Coordinates')
geo_simple.crs = {'init' :'epsg:4326'}

#Generar cruce geográfico
geo_j = gpd.sjoin(geo_simple, polygon1, how="left", op="intersects")
# Unión del resultado geográfico con los demás datos.
geo = pd.merge(dwc_co, geo_j, on='gbifID', how = 'left')
#Filtre por nombre columna los elementos que no hayan quedado vacíos, ese será el resultado del cruce geográfico. Remplace NombreColumna por el nombre del alguna columna del shapefile
geo_r = geo[geo.NombreColumna.notnull()]



#---------------------------------------------5. Consulta por año del evento-----------------------------------------------------------

#Si quiere cambiar el parametro para buscar una fecha exacta (Ej: 2022-02-09) debe cambiar el parámetro de la columna 'year' por 'eventDate'

    ### Un único año
eventDate-year = dwc_co[(dwc_co['year'] == f)]
    ### VARIOS años
eventDate-year = dwc_co[dwc_co['year'].isin(f)]



#---------------------------------------------6. Consulta por DOI-----------------------------------------------------------



    ### Un ÚNICO doi
DOI = dwc_co[(dwc_co['doi']== doi)]
    ### VARIOS dois
DOI = dwc_co[dwc_co['doi'].isin(doi)]


#--------------------------------------------------------------------------------------------------------------------------------------


#TIPO DE RESPUESTA

#Elija según corresponda
Resultado = dwc_co
Resultado = taxo #1 
Resultado = dpto #2
Resultado = mpios #3
Resultado = geo_r # 4
Resultado = eventDate-year #5
Resultado = DOI # 6
Resultado = locality


#A: Simplificada (Sin información de especies endémicas, amenazadas, exóticas y migratorias )
Resultado = Resultado[['gbifID','doi','organization','datasetTitle','license','occurrenceID','basisOfRecord','type','collectionCode','catalogNumber','datasetID','recordedBy','individualCount','organismQuantity','organismQuantityType','eventID','samplingProtocol','eventDate','year','month','day','countryCode','country','stateProvince','county','habitat','municipality','locality','elevation','depth','coordinateUncertaintyInMeters','decimalLatitude','decimalLongitude','Departamento-ubicacionCoordenada','Municipio-ubicacionCoordenada','ZonaMaritima','stateProvinceValidation','countyValidation','flagGEO','scientificName','kingdom','phylum','class','order','family','genus','species','taxonRank','identifiedBy','dateIdentified','identificationQualifier','vernacularName','flagTAXO','datasetKey','publishingOrgKey','created','logoUrl','dataType','publishingCountry','repatriated','issue' ]]

#B: Completa (Inlcuye información de especies endémicas, amenazadas, exóticas y migratorias)
Resultado = Resultado[['gbifID','doi','organization','datasetTitle','license','occurrenceID','basisOfRecord','type','collectionCode','catalogNumber','datasetID','recordedBy','individualCount','organismQuantity','organismQuantityType','eventID','samplingProtocol','eventDate','year','month','day','countryCode','country','stateProvince','county','habitat','municipality','locality','elevation','depth','coordinateUncertaintyInMeters','decimalLatitude','decimalLongitude','Departamento-ubicacionCoordenada','Municipio-ubicacionCoordenada','ZonaMaritima','stateProvinceValidation','countyValidation','flagGEO','scientificName','kingdom','phylum','class','order','family','genus','species','taxonRank','identifiedBy','dateIdentified','identificationQualifier','vernacularName','appendixCITES','threatStatus_UICN','threatStatus_MADS','isInvasive_mads','isInvasive_griis','exotic','listaReferenciaCO','endemic','migratory','isTerrestrial','isFreshwater','isMarine','isBrackish','isHybrid','flagTAXO','datasetKey','publishingOrgKey','created','logoUrl','dataType','publishingCountry','repatriated','issue']]

#C: Específica (Solo incluir campos según sean solicitados)
Resultado = Resultado[['gbifID','doi','organization','datasetTitle','license','occurrenceID','basisOfRecord','type','collectionCode','catalogNumber','datasetID','recordedBy','individualCount','organismQuantity','organismQuantityType','eventID','samplingProtocol','eventDate','year','month','day','countryCode','country','stateProvince','county','habitat','municipality','locality','elevation','depth','coordinateUncertaintyInMeters','decimalLatitude','decimalLongitude','Departamento-ubicacionCoordenada','Municipio-ubicacionCoordenada','ZonaMaritima','stateProvinceValidation','countyValidation','flagGEO','scientificName','kingdom','phylum','class','order','family','genus','species','taxonRank','identifiedBy','dateIdentified','identificationQualifier','vernacularName','appendixCITES','threatStatus_UICN','threatStatus_MADS','isInvasive_mads','isInvasive_griis','exotic','listaReferenciaCO','endemic','migratory','isTerrestrial','isFreshwater','isMarine','isBrackish','isHybrid','flagTAXO','datasetKey','publishingOrgKey','created','logoUrl','dataType','publishingCountry','repatriated','issue', 'nombre','id_pnn','categoria']]

#Nombre del archivo de salida
x ="RRBB_[NombreDescriptivoDeLaConsulta]_[Fecha].txt" # Recuerde eliminar las llaves [].
x ="RRBB_CruceGeopandasCoMarinos_20220302.txt" # Recuerde eliminar las llaves [].

#EXPORTAR RESULTADOS
Resultado.to_csv('C:\\Consultas\\Respuestas\\' + x, sep="\t", encoding = "utf8")


#----------------------------------------------------------------------------------------------------------------------------------------------




